
//Implementing a Stack ADT With a List ADT

#ifndef STACK_H
#define STACK_H


#include "List.h"

class Stack
{
private:

   List data;

public:

   int size();
   
   void push(int);

   void pop();

   int top();

   void clear();

};

#endif
