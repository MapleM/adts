#include <iostream>
#include <random>
#include "List.h"

using namespace std;

int main()
{
	List L1, L2; //Declare two list objects, L1 and L2
	
	cout << "Welcome to my List ADT client"<<endl<<endl;
	
	//Do some stuff with L1, L2, ... (Eg. cout<<L1.size();)
	// ...
	
	mt19937 eng1(102);
	std::uniform_int_distribution<> unifrm(10,20);
	int r = 0;
	
	for(int i = 0; i < 10; i++)
	{
		r = unifrm(eng1);
		L1.insert(r,(L1.size() + 1));
	}
 
	cout << L1.size() << endl << endl;
	
	
	
	for(int i = 1; i < (L1.size() + 1); i++)
	{
		
		cout << L1.get(i) << endl;
		
	}
	
	L1.clear();
 
	cout << endl << L1.size() << endl;
	
	for(int i = 1; i < (L1.size() + 1); i++)
	{
		
		cout << L1.get(i) << endl;
		cout << "f-";
	}
	
	

}
