#include "StackL.h"

using namespace std;

int Stack::size()
 {
	return data.size(); 
 }

void Stack::push(int val)
{
	data.insert(val,1);
}

void Stack::pop()
{
	data.remove(1);
}
	
int Stack::top()
{
	return data.get(1);
}

void Stack::clear()
{
	data.clear(); 
	/*while(data.size() > 0)            //This is technically the correct way todo it 
	{
		pop();
	}*/
}
