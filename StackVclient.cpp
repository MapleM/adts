#include <iostream> 
#include <string> 
#include <random>
#include "StackV.h"//Use vector implementation of Stack

using namespace std;
        
        
string reverseString(string str)
{
     string res;
     Stack stk;

   for (auto ch : str ) 
      stk.push(ch);

    
    while( stk.size() > 0)
       {
	  res +=  stk.top();
	   stk.pop();
       }

   return res;
}


int main()
{         
    string a_string = "COMP2115 here we come!!!";
     
    cout<<"\""<<a_string<<"\""<<endl<<"\""<<reverseString(a_string)<<"\"" << endl << endl;
	    	    
	Stack S1;
	
	mt19937 eng1(102);
	std::uniform_int_distribution<> unifrm(10,20);
	int r = 0;
	
	for(int i = 0; i < 10; i++)
	{
		r = unifrm(eng1);
		S1.push(r);
		cout << S1.top() << endl;
	}
	
	cout << endl;
	
	for(int i = 0; i < 3; i++)
	{
		cout << S1.top() << " is at the top of the list and " << S1.size() << " is the size of the list." << endl;
		S1.pop();
	}
	
	cout << S1.top() << " is at the top of the list and " << S1.size() << " is the size of the list." << endl;
	
	S1.clear();
	
	cout << S1.top() <<  " is at the top of the list and "  << S1.size() << " is the size of the list."  << endl;
	    
    return 0;
        
}
